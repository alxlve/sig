package fr.lavie.alexis.sign.backend.rest;

@Path("/map/layer")
public class LayerService {

    @GET
    @Path("/parking")
    @Produces(MediaType.APPLICATION_JSON)
    public Response parking() {
        geometry = "way";
        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "amenity='parking'";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , ST_AsGeoJSON(" + geometry + ")::json As geometry\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

}
