function loadLayers() {

	var getText = function(feature, resolution) {
		var maxResolution = 2;
		var text = feature.get('name');

		if (resolution > maxResolution) {
			text = '';
		}

		return text;
	};

	var textFunction = function (feature, resolution) {
		var text = new ol.style.Text({
			text:getText(feature, resolution),
			fontFamily: "Verdana",
			fontSize: 12,
			fill: new ol.style.Fill({
				color: 'white'
			}),
			stroke: new ol.style.Stroke({
				color: 'black',
				width: 2
			})
		});
		return text;
	}

	var autresFill = new ol.style.Fill({
		color: 'rgba(136, 136, 136, ' + vectorLayersOpacity + ')'
	});

	var autresStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var autresStyleFunction = function (feature, resolution) {
		var autresStyle = new ol.style.Style({
			fill: autresFill,
			stroke: autresStroke,
			text: textFunction(feature, resolution)
		});
		return [autresStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/autres', function (data) {
		autresLayer = new ol.layer.Vector({
			id: 'autresLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: autresStyleFunction
		});
		map.addLayer(autresLayer);
	});

	var bibliothequeFill = new ol.style.Fill({
		color: 'rgba(0, 167, 179, ' + vectorLayersOpacity + ')'
	});

	var bibliothequeStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var bibliothequeStyleFunction = function (feature, resolution) {
		var bibliothequeStyle = new ol.style.Style({
			fill: bibliothequeFill,
			stroke: bibliothequeStroke,
			text: textFunction(feature, resolution)
		});
		return [bibliothequeStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/bibliotheque', function (data) {
		bibliothequeLayer = new ol.layer.Vector({
			id: 'bibliothequeLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: bibliothequeStyleFunction
		});
		map.addLayer(bibliothequeLayer);
	});

	var collegiumDEGFill = new ol.style.Fill({
		color: 'rgba(226, 8, 0, ' + vectorLayersOpacity + ')'
	});

	var collegiumDEGStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var collegiumDEGStyleFunction = function (feature, resolution) {
		var collegiumDEGStyle = new ol.style.Style({
			fill: collegiumDEGFill,
			stroke: collegiumDEGStroke,
			text: textFunction(feature, resolution)
		});
		return [collegiumDEGStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/collegium-deg', function (data) {
		collegiumDEGLayer = new ol.layer.Vector({
			id: 'collegiumDEGLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: collegiumDEGStyleFunction
		});
		map.addLayer(collegiumDEGLayer);
	});

	var collegiumLLSHFill = new ol.style.Fill({
		color: 'rgba(255, 126, 0, ' + vectorLayersOpacity + ')'
	});

	var collegiumLLSHStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var collegiumLLSHStyleFunction = function (feature, resolution) {
		var collegiumLLSHStyle = new ol.style.Style({
			fill: collegiumLLSHFill,
			stroke: collegiumLLSHStroke,
			text: textFunction(feature, resolution)
		});
		return [collegiumLLSHStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/collegium-llsh', function (data) {
		collegiumLLSHLayer = new ol.layer.Vector({
			id: 'collegiumLLSHLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: collegiumLLSHStyleFunction
		});
		map.addLayer(collegiumLLSHLayer);
	});

	var collegiumSTFill = new ol.style.Fill({
		color: 'rgba(163, 0, 123, ' + vectorLayersOpacity + ')'
	});

	var collegiumSTStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var collegiumSTStyleFunction = function (feature, resolution) {
		var collegiumSTStyle = new ol.style.Style({
			fill: collegiumSTFill,
			stroke: collegiumSTStroke,
			text: textFunction(feature, resolution)
		});
		return [collegiumSTStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/collegium-st', function (data) {
		collegiumSTLayer = new ol.layer.Vector({
			id: 'collegiumSTLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: collegiumSTStyleFunction
		});
		map.addLayer(collegiumSTLayer);
	});

	var iutFill = new ol.style.Fill({
		color: 'rgba(0, 140, 0, ' + vectorLayersOpacity + ')'
	});

	var iutStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var iutStyleFunction = function (feature, resolution) {
		var iutStyle = new ol.style.Style({
			fill: iutFill,
			stroke: iutStroke,
			text: textFunction(feature, resolution)
		});
		return [iutStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/iut', function (data) {
		iutLayer = new ol.layer.Vector({
			id: 'iutLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: iutStyleFunction
		});
		map.addLayer(iutLayer);
	});

	var lacFill = new ol.style.Fill({
		color: 'rgba(0, 102, 255, ' + vectorLayersOpacity + ')'
	});

	var lacStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var lacStyleFunction = function (feature, resolution) {
		var lacStyle = new ol.style.Style({
			fill: lacFill,
			stroke: lacStroke,
			text: textFunction(feature, resolution)
		});
		return [lacStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/lac', function (data) {
		lacLayer = new ol.layer.Vector({
			id: 'lacLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: lacStyleFunction
		});
		map.addLayer(lacLayer);
	});

	var parkingFill = new ol.style.Fill({
		color: 'rgba(128, 179, 255, ' + vectorLayersOpacity + ')'
	});

	var parkingStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var parkingStyleFunction = function (feature, resolution) {
		var parkingStyle = new ol.style.Style({
			fill: parkingFill,
			stroke: parkingStroke,
			text: textFunction(feature, resolution)
		});
		return [parkingStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/parking', function (data) {
		parkingLayer = new ol.layer.Vector({
			id: 'parkingLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: parkingStyleFunction
		});
		map.addLayer(parkingLayer);
	});

	var polytechFill = new ol.style.Fill({
		color: 'rgba(255, 213, 0, ' + vectorLayersOpacity + ')'
	});

	var polytechStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var polytechStyleFunction = function (feature, resolution) {
		var polytechStyle = new ol.style.Style({
			fill: polytechFill,
			stroke: polytechStroke,
			text: textFunction(feature, resolution)
		});
		return [polytechStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/polytech', function (data) {
		polytechLayer = new ol.layer.Vector({
			id: 'polytechLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: polytechStyleFunction
		});
		map.addLayer(polytechLayer);
	});

	var residencesFill = new ol.style.Fill({
		color: 'rgba(139, 179, 0, ' + vectorLayersOpacity + ')'
	});

	var residencesStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var residencesStyleFunction = function (feature, resolution) {
		var residencesStyle = new ol.style.Style({
			fill: residencesFill,
			stroke: residencesStroke,
			text: textFunction(feature, resolution)
		});
		return [residencesStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/residences', function (data) {
		residencesLayer = new ol.layer.Vector({
			id: 'residencesLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: residencesStyleFunction
		});
		map.addLayer(residencesLayer);
	});

	var restaurantFill = new ol.style.Fill({
		color: 'rgba(117, 81, 26, ' + vectorLayersOpacity + ')'
	});

	var restaurantStroke = new ol.style.Stroke({
		color: 'rgba(0, 0, 0, 0.4)'
	});

	var restaurantStyleFunction = function (feature, resolution) {
		var restaurantStyle = new ol.style.Style({
			fill: restaurantFill,
			stroke: restaurantStroke,
			text: textFunction(feature, resolution)
		});
		return [restaurantStyle];
	};

	$.getJSON(serverUrl + '/rest/map/layer/restaurant', function (data) {
		restaurantLayer = new ol.layer.Vector({
			id: 'restaurantLayer',
			source: new ol.source.Vector({
				features: (new ol.format.GeoJSON()).readFeatures(data)
			}),
			style: restaurantStyleFunction
		});
		map.addLayer(restaurantLayer);
	});

}

$(document).ready(function() {
	$('input[type=checkbox].layer').on('change', function() {
		var layer = {
			autresLayer: autresLayer,
			bibliothequeLayer: bibliothequeLayer,
			collegiumDEGLayer: collegiumDEGLayer,
			collegiumLLSHLayer: collegiumLLSHLayer,
			collegiumSTLayer: collegiumSTLayer,
			iutLayer: iutLayer,
			lacLayer: lacLayer,
			parkingLayer: parkingLayer,
			polytechLayer: polytechLayer,
			residencesLayer: residencesLayer,
			restaurantLayer: restaurantLayer
		}[$(this).attr('id')];
		layer.setVisible(!layer.getVisible());
	});
});
