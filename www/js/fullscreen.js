function switchFullscreen() {
    if ($.fullscreen.isFullScreen()) {
        closeFullscreen();
    } else {
        openFullscreen();
    }
}

function openFullscreen() {
    console.log('Opening Fullscreen...');
    $('body').fullscreen();
}

function closeFullscreen() {
    console.log('Closing Fullscreen...');
    $.fullscreen.exit();
}

$(function() {
    $('.switchFullscreen').click(function() {
        switchFullscreen();
    });
    $('.openFullscreen').click(function() {
        openFullscreen();
    });
    $('.closeFullscreen').click(function() {
        closeFullscreen();
    });
});
