var startPoint = new ol.Feature();
var endPoint = new ol.Feature();
var transform = ol.proj.getTransform('EPSG:3857', 'EPSG:4326');
var pointsLayer = null;
var routingLayer = null;

function clearRouting() {
	startPoint.setGeometry(null);
	endPoint.setGeometry(null);
	map.removeLayer(pointsLayer);
	map.removeLayer(routingLayer);
	pointsLayer = null;
	routingLayer = null;
}

function routing(evt) {
	if (pointsLayer == null) {
		pointsLayer = new ol.layer.Vector({
			source: new ol.source.Vector({
				features: [startPoint, endPoint]
			})
		});
		map.addLayer(pointsLayer);
	}

	if (routingLayer != null) {
		startPoint.setGeometry(null);
		endPoint.setGeometry(null);
		map.removeLayer(routingLayer);
		routingLayer = null;
		// console.log('reset');
	}

	if (startPoint.getGeometry() == null) {
		// First click.
		startPoint.setGeometry(new ol.geom.Point(evt.coordinate));
		
	} else if (endPoint.getGeometry() == null) {
		// Second click.
		endPoint.setGeometry(new ol.geom.Point(evt.coordinate));

		// Transform the coordinates from the map projection (EPSG:3857)
		// to the server projection (EPSG:4326).
		var startCoord = transform(startPoint.getGeometry().getCoordinates());
		var destCoord = transform(endPoint.getGeometry().getCoordinates());
		x1 = startCoord[0];
		y1 = startCoord[1];
		x2 = destCoord[0];
		y2 = destCoord[1];

		var routingStroke = new ol.style.Stroke({
			color: 'rgba(200, 0, 0, 0.8)',
			width: 3
		});

		var routingStyle = new ol.style.Style({
			stroke: routingStroke,
		});

		var url = serverUrl + '/rest/map/routing' + "?x1=" + x1 + "&y1=" + y1 + "&x2=" + x2 + "&y2=" + y2;

		$.ajax({
			url: url,
			async: false,
			dataType: 'json',
			success: function (data) {
				routingLayer = new ol.layer.Vector({
					source: new ol.source.Vector({
						features: (
							// Transform the coordinates from the server projection (EPSG:4326)
							// to the server projection (EPSG:3857).
							new ol.format.GeoJSON({
								defaultDataProjection: 'EPSG:4326'
							})).readFeatures(data, {
								dataProjection: 'EPSG:4326',
									featureProjection: 'EPSG:3857'
								}),
					}),
					style: routingStyle
				});
				map.addLayer(routingLayer);
			}
		});
	}
}
