/**
 * @constructor
 * @extends {ol.control.Control}
 * @param {Object=} opt_options Control options.
 */
RotateNorthControl = function(opt_options) {
	var options = opt_options || {};

	var button = document.createElement('button');
	button.innerHTML = 'N';

	var this_ = this;
	var handleRotateNorth = function(e) {
		this_.getMap().getView().setRotation(0);
	};

	button.addEventListener('click', handleRotateNorth, false);
	button.addEventListener('touchstart', handleRotateNorth, false);

	var element = document.createElement('div');
	element.className = 'rotate-north ol-unselectable ol-control';
	element.appendChild(button);

	ol.control.Control.call(this, {
		element: element,
		target: options.target
	});

};
ol.inherits(RotateNorthControl, ol.control.Control);

function loadMap() {
	map = new ol.Map({
		target: 'map',
		renderer: 'canvas',
		controls: ol.control.defaults({attributionOptions: ({collapsible: true, target: document.getElementById('copyright'),})}).extend([
			// new ol.control.FullScreen(),
			new RotateNorthControl(),
		]),
		interactions: ol.interaction.defaults().extend([
				new ol.interaction.DragRotateAndZoom()
		]),
		layers : [
		new ol.layer.Group({
			'title': 'Carte de base',
			layers: [
				new ol.layer.Tile({
					title: 'OpenStreetMap',
					type: 'base',
					visible: true,
					source: new ol.source.OSM(),
					opacity: baseLayerOpacity
				}),
				new ol.layer.Tile({
					title: 'OpenCycleMap',
					type: 'base',
					visible: false,
					source: new ol.source.OSM({
						attributions: [
							new ol.Attribution({
								html: 'All maps &copy; ' +
								'<a href="http://www.opencyclemap.org/">OpenCycleMap</a>'
							}),
							ol.source.OSM.ATTRIBUTION
						],
						url: 'http://{a-c}.tile.opencyclemap.org/cycle/{z}/{x}/{y}.png'
					}),
					opacity: baseLayerOpacity
				}),
				new ol.layer.Tile({
					title: 'MapQuest Road',
					type: 'base',
					visible: false,
					style: 'Road',
					source: new ol.source.MapQuest({layer: 'osm'})
				}),
				new ol.layer.Tile({
					title: 'BingMaps Aerial',
					type: 'base',
					visible: false,
					source: new ol.source.BingMaps({culture: 'fr-fr', key: 'Ak-dzM4wZjSqTlzveKz5u0d4IQ4bRzVI309GxmkgSVr1ewS6iPSrOvOKhA-CJlm3', imagerySet: 'Aerial'}),
					maxZoom: 19,
					opacity: baseLayerOpacity
				})
			]
		})
		],
		view: view
	});

	var zoomslider = new ol.control.ZoomSlider();
	map.addControl(zoomslider);

	var layerSwitcher = new ol.control.LayerSwitcher({
		tipLabel: 'Légende'
	});

	map.addControl(layerSwitcher);
}

$(document).ready(function() {
	$('input[type=checkbox].baseLayer').on('change', function() {
		var baseLayer = {
			autresLayer: autresLayer,
			bibliothequeLayer: bibliothequeLayer,
			collegiumDEGLayer: collegiumDEGLayer,
			collegiumLLSHLayer: collegiumLLSHLayer,
			collegiumSTLayer: collegiumSTLayer,
			iutLayer: iutLayer,
			lacLayer: lacLayer,
			parkingLayer: parkingLayer,
			polytechLayer: polytechLayer,
			residencesLayer: residencesLayer,
			restaurantLayer: restaurantLayer
		}[$(this).attr('id')];
		if($(this).is(':checked')) {
			baseLayer.setVisible(true);
		} else {
			baseLayer.setVisible(false);
		}
	});
});
