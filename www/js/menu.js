function loadMenu() {
	$('nav#menu').mmenu({
		extensions	: [ 'effect-slide-menu', 'pageshadow' ],
		searchfield	: {
			noResults: 'Aucun résultat trouvé',
			placeholder : 'Recherche'
		},
		counters	: true,
		navbar 		: {
			title		: 'Menu'
		},
		navbars		: [
		{
			position	: 'top',
			content		: [ 'searchfield' ],
		}, {
			position	: 'top',
			content		: [
			'prev',
			'title',
			'close'
			]
		}, {
			position	: 'bottom',
			content		: [ '<a href="http://www.univ-orleans.fr/">Université d\'Orléans</a>' ]
		}
		]
	});
}

function preloadMenu() {
	loadPoints();
	loadPolygons();
	setTimeout(function(){loadMenu()}, 1000);
}

function loadPoints() {
	$.getJSON(serverUrl + '/rest/map/locate/points', function (data) {
		var toWrite = '';
		var previousName = '';
		// console.log(data.features);

		$.each(data.features, function(i, item) {
			var name = data.features[i].properties.name;
			var id = data.features[i].properties.osm_id;

			// Pour la recherche associée à la description :
			var refs = data.features[i].properties.about;
			if (name != previousName) {
				toWrite += '<li><a href="admin.html?id=' + id + '" target="_blank">' + name + '</a><span style="display:none;">' + refs + '</span><input id="' + id + '" type="checkbox" class="Toggle point" onclick="clickLocatePoint()"/></li>';
			}
			previousName = name;
		});
		$('#menuPoints').html(toWrite);
	});
}

function loadLayerPolygons(layer) {
	var toWrite = '';
	$.ajax({
		url: serverUrl + '/rest/map/locate/polygons/' + layer,
		async: false,
		dataType: 'json',
		success: function (data) {
			$.each(data.features, function(i, item) {
				var name = data.features[i].properties.name;
				var id = data.features[i].properties.osm_id;

				var refs = data.features[i].properties.about;
				if (typeof name != 'undefined') {
					toWrite += '<li><a href="admin.html?id=' + id + '" target="_blank">' + name + '</a><input id="' + id + '" type="checkbox" class="Toggle polygon" unchecked onclick="clickLocatePolygon()"/></li>';
				}
			});
		}
	});
	return toWrite;
}

function loadPolygons() {
	$('#menuPolygonsAutres').html(loadLayerPolygons('autres'));
	$('#menuPolygonsBibliotheque').html(loadLayerPolygons('bibliotheque'));
	$('#menuPolygonsCollegiumDEG').html(loadLayerPolygons('collegium-deg'));
	$('#menuPolygonsCollegiumLLSH').html(loadLayerPolygons('collegium-llsh'));
	$('#menuPolygonsCollegiumST').html(loadLayerPolygons('collegium-st'));
	$('#menuPolygonsIUT').html(loadLayerPolygons('iut'));
	$('#menuPolygonsPolytech').html(loadLayerPolygons('polytech'));
	$('#menuPolygonsResidences').html(loadLayerPolygons('residences'));
	$('#menuPolygonsRestaurant').html(loadLayerPolygons('restaurant'));
}
