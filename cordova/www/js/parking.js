var point = new ol.Feature();
var geolocationPoint = null;
var transform = ol.proj.getTransform('EPSG:3857', 'EPSG:4326');
var pointLayer = null;
var parkingLocationLayer = null;

function clearParking() {
	point.setGeometry(null);
	map.removeLayer(pointLayer);
	map.removeLayer(parkingLocationLayer);
	pointLayer = null;
	parkingLocationLayer = null;
}

function parkingFromPoint(sourcePoint) {
	point.setGeometry(sourcePoint);

	var coord = point.getGeometry().getCoordinates();
	x = coord[0];
	y = coord[1];

	var parkingLocationFill = new ol.style.Fill({
		color: 'rgba(0, 0, 0, ' + vectorLayersOpacity + ')'
	});

	var parkingLocationStroke = new ol.style.Stroke({
		color: 'rgba(255, 255, 255, 0.8)',
		width: 3
	});

	var parkingLocationStyle = new ol.style.Style({
		fill: parkingLocationFill,
		stroke: parkingLocationStroke
	});

	var url = serverUrl + '/rest/map/parking' + "?x=" + x + "&y=" + y;

	$.ajax({
		url: url,
		async: false,
		dataType: 'json',
		success: function (data) {
			parkingLocationLayer = new ol.layer.Vector({
				source: new ol.source.Vector({	
					features: (new ol.format.GeoJSON()).readFeatures(data)
				}),
				style: parkingLocationStyle
			});
			map.addLayer(parkingLocationLayer);
		}
	});
}

function parking() {
	if (pointLayer == null) {
		pointLayer = new ol.layer.Vector({
			source: new ol.source.Vector({
				features: [point]
			})
		});
		map.addLayer(pointLayer);
	}

	if (parkingLocationLayer != null) {
		point.setGeometry(null);
		map.removeLayer(parkingLocationLayer);
		parkingLocationLayer = null;
		// console.log('reset');
	}
}

function parkingPoint(evt) {
	parking();
	if (point.getGeometry() == null) {
		parkingFromPoint(new ol.geom.Point(evt.coordinate));
	}
}

function parkingGeolocation() {
	parking();
	if (point.getGeometry() == null) {
		parkingFromPoint(geolocationPoint);
	}
}

$(document).ready(function() {
	var geolocation = new ol.Geolocation({
  		projection: view.getProjection()
	});
	geolocation.setTracking(true);
	geolocation.on('change:position', function() {
		geolocationPoint = new ol.geom.Point(geolocation.getPosition());
		if (mapMode == 'nearParkingFromGeolocation') {
			parkingGeolocation();
		}
	});
});
