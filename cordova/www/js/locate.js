function clickLocatePoint() {
	$('input[type=checkbox].point').on('change', function(event) {
		event.stopImmediatePropagation();
		var id = $(this).attr('id');
		if($(this).is(':checked')) {
			$.getJSON(serverUrl + '/rest/map/locate/point?id=' + id, function (data) {
				var layer = new ol.layer.Vector({
					id: parseInt(id),
					source: new ol.source.Vector({
						features: (new ol.format.GeoJSON()).readFeatures(data)
					}),
					style: new ol.style.Style({
						image: new ol.style.Icon(({
							anchor: [0.5, 40],
							anchorXUnits: 'fraction',
							anchorYUnits: 'pixels',
							src: 'img/marker-icon.png'
						}))
					})
				});
				map.addLayer(layer);
			});
		} else {
			var layer;
			map.getLayers().forEach(function (lyr) {
				if (id == lyr.get('id')) {
					layer = lyr;
				}            
			});
			layer.setVisible(false);
		}
	});
}

function clickLocatePolygon() {
	$('input[type=checkbox].polygon').on('change', function(event) {
		event.stopImmediatePropagation();
		var id = $(this).attr('id');
		if($(this).is(':checked')) {
			$.getJSON(serverUrl + '/rest/map/locate/polygon?id=' + id, function (data) {
				var layer = new ol.layer.Vector({
					id: parseInt(id),
					source: new ol.source.Vector({
						features: (new ol.format.GeoJSON()).readFeatures(data)
					}),
					style: new ol.style.Style({
						image: new ol.style.Icon(({
							anchor: [0.5, 40],
							anchorXUnits: 'fraction',
							anchorYUnits: 'pixels',
							src: 'img/marker-icon.png'
						}))
					})
				});
				map.addLayer(layer);
			});
		} else {
			var layer;
			map.getLayers().forEach(function (lyr) {
				if (id == lyr.get('id')) {
					layer = lyr;
				}            
			});
			if (typeof layer != 'undefined') {
				layer.setVisible(false);
			}
		}
	});
}
