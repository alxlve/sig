function resetMap() {
	console.log('Resetting the map;');
	map.getView().setCenter(univCenter);
	map.getView().setRotation(univRotation);
	map.getView().setZoom(univZoom);
}

function setMode(mode) {
 	mapMode = mode;
 	console.log("Using the: '" + mode + "' mode;")
 	clearParking();
 	clearRouting();
}

$(function() {
    $('.resetMap').click(function() {
        resetMap();
    });
});
