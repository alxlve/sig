sudo rm -rf /var/lib/postgres/*
sudo -i -u postgres
initdb --locale fr_FR.UTF-8 -E UTF8 -D '/var/lib/postgres/data'
createuser --interactive -P
    [postgis][postgis][postgis][o]

psql
# Verify available extensions
SELECT name, default_version,installed_version
FROM pg_available_extensions WHERE name LIKE 'postgis%' ;
\q

createdb projet -U postgis -W
psql -U postgis -W projet
# Install extension for spatial database named projet
\c projet
CREATE EXTENSION postgis;
CREATE EXTENSION postgis_topology;
CREATE EXTENSION fuzzystrmatch;
CREATE EXTENSION postgis_tiger_geocoder;
CREATE EXTENSION hstore;

createdb routing -U postgis -W
psql -U postgis -W routing
\c routing
CREATE EXTENSION postgis;
CREATE EXTENSION pgrouting;
