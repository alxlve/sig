#! /bin/bash

dir="files/"
username="postgis"
password="postgis"
database="projet"

wget http://api.openstreetmap.org/api/0.6/map?bbox=1.9232,47.8391,1.9466,47.8591 -O ${dir}univ.osm
# wget http://overpass.osm.rambler.ru/cgi/xapi_meta?*[bbox=1.9232,47.8391,1.9466,47.8591] -O ${dir}univ.osm
psql -U ${username} -d ${database} -a -f ${dir}univ_boundaries.sql
osm2pgsql -c -H localhost -U ${username} -d ${database} --slim -S ${dir}default.style --hstore ${dir}univ.osm
# psql -U ${username} -d ${database} -a -f ${dir}merge.sql
psql -U ${username} -d ${database} -a -f ${dir}after_import.sql
psql -U ${username} -d ${database} -a -f ${dir}clean.sql

/usr/share/bin/osm2pgrouting -file ${dir}univ.osm -conf /usr/share/osm2pgrouting/mapconfig.xml -dbname routing -user ${username} -host localhost -clean
