-- Script osm2pgsql

DELETE from "planet_osm_roads" r1
WHERE r1.osm_id not in (
	SELECT r.osm_id
	FROM "planet_osm_roads" r JOIN univ_boundaries u
	ON ST_DWITHIN(u.geom, r.way, 1)
);
DELETE from "planet_osm_line" r1
WHERE r1.osm_id not in (
	SELECT r.osm_id
	FROM "planet_osm_line" r JOIN univ_boundaries u
	ON ST_DWITHIN(u.geom, r.way, 1)
);
DELETE from "planet_osm_polygon" r1
WHERE r1.osm_id not in (
	SELECT r.osm_id
	FROM "planet_osm_polygon" r JOIN univ_boundaries u
	ON ST_DWITHIN(u.geom, r.way, 1)
);
DELETE from "planet_osm_point" r1
WHERE r1.osm_id not in (
	SELECT r.osm_id
	FROM "planet_osm_point" r JOIN univ_boundaries u
	ON ST_DWITHIN(u.geom, r.way, 1)
);
