//création des popups
function popUps(feature, layer) {
    if (feature.properties)
        if (feature.properties.name) {

            layer.bindPopup(//bouton ajout service
                BOUTON + feature.properties.name + '</b>');

        }

        else if (feature.properties.amenity) {
            layer.bindPopup("Parking");
        }


}


function addService() {
    service = prompt("Service à ajouter");
    if (service != "" && service != null)
        layer1._popup.setContent(layer1._popup.getContent() +
        '</br><i style="color:' + B_CLAIR + '">&nbsp;&nbsp;' + service + '</i>');

}


function amenity(feature) {
    switch (feature.properties.amenity) {

        case "parking" :
            return PARKING;
        case "restaurant" :
            return RESTAURANT;
        case "library" :
            return LIBRARY;
        case "dormitory" :
            return DORMITORY;

        default :
            return POLYGONSTYLE;
    }
}

function filtre(feature, layer) {
    return feature.properties.name || feature.properties.amenity;
}


function onMapClick(e) {
    if(marker)
    map.removeLayer(marker);



        marker = new L.Marker(e.latlng, {icon: L.icon(MARKERIMAGE)});
        map.addLayer(marker);
        boolMarker = true;


        L.GeometryUtil.closestLayer(map, choix().getLayers(), e.latlng).layer.openPopup();

}

//désactive les fonctionnalités d'un bouton
function desative(icon) {
    icon.disable();
    map.off('click', onMapClick);
    activerMarker = false;

    if (icon == iconTarget)
        tActivé = false;
    else
        pActivé = false;

    if (!tActivé && !pActivé && boolMarker) {
        boolMarker = false;
        map.removeLayer(marker);
    }
}

//active les fonctionnalités d'un bouton
function active(icon) {
      icon.enable();
    map.on('click', onMapClick);
    activerMarker = true;

    if (icon == iconTarget)
        tActivé = true;
    else
        pActivé = true;

if(activerGeo)
    if(locali)
        L.GeometryUtil.closestLayer(map, choix().getLayers(), locali).layer.openPopup();//trouve closestParking(batiment)


    else if(locationPossible) {
        map.locate({setView: true, maxZoom: 16});

    }

}

function clicBoutons(icon) {


    if (icon == iconTarget && !tActivé) {//si on clic target et quil est pas déja activé

        desative(iconP);
        active(iconTarget);
    }
    else if (icon == iconTarget && tActivé)//si on clic target et quil est déja activé
        desative(iconTarget);

    else if (icon == iconP && !pActivé)//si on clic P et quil est pas déja activé
    {
        desative(iconTarget);
        active(iconP);
    }

    else if (icon == iconP && pActivé)//si on clic P et quil est déja activé
        desative(iconP);


}

//controle de la recherche
function addControl() {
    var aux = new L.Control.Search({layer: polygons, propertyName: 'name'});

    aux.on('search_locationfound', function (e) {
        if (pActivé)
            L.GeometryUtil.closestLayer(map, park.getLayers(), e.latlng).layer.openPopup();//trouve closestParking(batiment)
        else if (e.layer._popup)
            e.layer.openPopup();

    }).on('search_collapsed', function (e) {

        polygons.eachLayer(function (layer) {	//restaure par defaut
            polygons.resetStyle(layer);
        });
    });
    return aux;
}

function choix()
{

    if (tActivé)//si on souhaite trouver le bat le plus proche
        return polygons;
    else//si parking le plus proche
        return park;

}
