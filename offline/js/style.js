const P_WEIGHT=1;
const P_OPACITY=0.65;
const BLEU="#0c34e5";
const GRIS="#b7afaf";
const JAUNE="#d3d800";
const ROUGE="#eb1e1e";
const MARRON="#7b4316";
const VERT="#4cac34";
const B_CLAIR="#80BFFF";
const BLANC="#ffffff";

const BOUTON='<button class="ui-btn ui-shadow ui-corner-all ui-icon-plus ui-btn-icon-notext" ' +
    'onclick="addService()"></button><b>';

const POLYGONSTYLE =
{
    "color": BLEU,
    "weight": P_WEIGHT,
    "opacity": P_OPACITY
};

const PARKING =
{
    "color": GRIS,
    "opacity": 0.4
};

const DORMITORY =
{
    "color": MARRON,
    "weight": P_WEIGHT,
    "opacity": P_OPACITY
};

const LIBRARY =
{
    "color": JAUNE,
    "weight": P_WEIGHT,
    "opacity": P_OPACITY
};

const RESTAURANT =
{
    "color": ROUGE,
    "weight": P_WEIGHT,
    "opacity": P_OPACITY
};

const INUTILESTYLE =
{
    "color": "#897676",
    "opacity": 1
};


const LINESTYLE =
{
    "color": ROUGE,
    "weight": P_WEIGHT,
    "opacity": 0.9
};


const MARKERIMAGE =
{
    iconUrl: 'img/marker.png',
    iconSize: [38, 55]
};