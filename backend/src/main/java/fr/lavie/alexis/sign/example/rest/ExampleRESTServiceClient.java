package fr.lavie.alexis.sign.example.rest;

import fr.lavie.alexis.sign.backend.config.Web;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

public class ExampleRESTServiceClient {

    public static void main(String[] args) {
        String stringJSON = "{\n" +
                            "    \"subject\": {\n" +
                            "        \"id\": \"1\",\n" +
                            "        \"topic\": \"REST Service\",\n" +
                            "        \"description\": \"This is a REST Service Example.\"\n" +
                            "    }\n" +
                            "}";

        JSONObject jsonObject = new JSONObject(stringJSON);
        System.out.println(jsonObject);

        try {
            URL url = new URL(Web.restUrl + "exampleRESTService");
            URLConnection connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
            out.write(jsonObject.toString());
            out.close();

            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            while (in.readLine() != null) {
            }
            System.out.println("\nExampleRESTService Invoked Successfully.");
            in.close();
        } catch (Exception e) {
            System.out.println("\nError while calling ExampleRESTService.");
            System.out.println(e);
        }
    }

}
