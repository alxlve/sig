package fr.lavie.alexis.sign.backend.rest;

import fr.lavie.alexis.sign.backend.config.Database;
import fr.lavie.alexis.sign.backend.json.JsonHelper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Path("/map/parking")
public class ParkingService {

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response routing(@DefaultValue("0") @QueryParam("x") BigDecimal x,
                            @DefaultValue("0") @QueryParam("y") BigDecimal y) {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String function = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;
        String order_by = null;
        String query = null;

        System.out.println("Parking | x: " + x + ", y: " + y + ";");

        geometry = "way";
        attributes = "osm_id";
        table = "planet_osm_polygon";
        where = "lg.amenity='parking'";
        order_by = "(ST_MakePoint(" + x + ", " + y + ")::geometry) <#> lg.way ASC LIMIT 1";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            query = "SELECT row_to_json(fc)\n" +
                    " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                    " FROM (SELECT 'Feature' As type\n" +
                    "    , ST_AsGeoJSON(" + geometry + ")::json As geometry\n" +
                    "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                    "      )) As properties\n" +
                    "   FROM " + table + " As lg WHERE " + where + " ORDER BY " + order_by + " ) As f ) As fc;";
//            System.out.println(query);
            result = sql.executeQuery(query);
            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

}
