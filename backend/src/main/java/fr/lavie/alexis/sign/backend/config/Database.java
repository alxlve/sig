package fr.lavie.alexis.sign.backend.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

    private static final String databaseDriver = "jdbc:postgresql";
    private static final String databaseHostIp = "localhost";
    private static final String databaseHostPort = "5432";
    private static final String databaseHost = databaseDriver + "://" + databaseHostIp + ":" + databaseHostPort + "/";
    private static final String databaseName = "projet";
    public static final String url = databaseHost + databaseName;
    private static final String databaseRoutingName = "routing";
    public static final String urlRouting = databaseHost + databaseRoutingName;
    public static final String username = "postgis";
    public static final String password = "postgis";
    private static Connection connection = null;

    public static Connection getConnection() throws SQLException {
        return Database.getConnection(null, null, null);
    }

    public static Connection getConnection(String url, String username, String password) throws SQLException {
        if (Database.connection == null) {
            if (url == null) {
                url = Database.url;
            }
            if (username == null) {
                username = Database.username;
            }
            if (password == null) {
                password = Database.password;
            }

            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            Database.connection = DriverManager.getConnection(url, username, password);
        }
        return Database.connection;
    }

    public static Connection getRoutingConnection() throws SQLException {
        try{
            Class.forName("org.postgresql.Driver");
        } catch(ClassNotFoundException e) {
            e.printStackTrace();
        }

        return DriverManager.getConnection(urlRouting, username, password);
    }

}
