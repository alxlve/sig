package fr.lavie.alexis.sign.example.postgresql;

import java.sql.*;

/*
    https://www.binpress.com/tutorial/introduction-to-postgis-part-1/24
 */
public class Country {

    public static void main(String[] args) {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;

        String url = "jdbc:postgresql://localhost:5432/TestGIS";
        String username = "postgis";
        String password = "postgis";

        try {
            connection = DriverManager.getConnection(url, username, password);
            sql = connection.createStatement();
//            result = sql.executeQuery("SELECT * FROM country");
            result = sql.executeQuery("SELECT id, name, ST_AsGeoJSON(boundary), name_alias FROM country WHERE id=1;");

            if (result.next()) {
                System.out.println(result.getString("id") + "," + result.getString("name") + "," + result.getString(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
