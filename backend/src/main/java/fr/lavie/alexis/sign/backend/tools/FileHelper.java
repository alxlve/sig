package fr.lavie.alexis.sign.backend.tools;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileHelper {

    public static String readFile(URL path, Charset encoding) throws IOException {
        byte[] encoded = new byte[0];
        try {
            encoded = Files.readAllBytes(Paths.get(path.toURI()));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return new String(encoded, encoding);
    }

}
