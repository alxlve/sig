package fr.lavie.alexis.sign.backend.rest;

import fr.lavie.alexis.sign.backend.config.Database;
import fr.lavie.alexis.sign.backend.json.JsonHelper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Path("/admin")
public class AdminService {

    @GET
    @Path("/get")
    @Produces(MediaType.APPLICATION_JSON)
    public Response locate(@QueryParam("id") BigInteger id) {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        geometry = "way";
        attributes = "name, about";
        table = "planet_osm_point";
        where = "osm_id = "+id;

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , ST_AsGeoJSON(" + geometry + ")::json As geometry\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/push")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllPoints(@QueryParam("id") BigInteger id,
                                  @QueryParam("name") String name,
                                  @QueryParam("about") String about
                                  ) {
        Connection connection = null;
        Statement sql = null;
        String json = null;

//        System.out.println(id);
//        System.out.println(name);
//        System.out.println(about);

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            String query ="UPDATE planet_osm_point " +
                    "SET name='" + name + "', about='" + about + "' " +
                    "WHERE osm_id='" + id + "';";
            sql.executeQuery(query);

            System.out.println(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

}
