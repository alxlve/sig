package fr.lavie.alexis.sign.example.rest;

import fr.lavie.alexis.sign.backend.config.Database;
import fr.lavie.alexis.sign.backend.json.JsonHelper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.sql.*;

@Path("/countryExample")
public class CountryService {

    /*
        Example : http://localhost:8080/rest/countryExample/country
     */
    @GET
    @Path("/country")
    @Produces(MediaType.APPLICATION_JSON)
    public Response country() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;

        json = new String();

        try {
            connection = Database.getConnection("jdbc:postgresql://localhost:5432/TestGIS", "postgis", "postgis");
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                    " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                    " FROM (SELECT 'Feature' As type\n" +
                    "    , ST_AsGeoJSON(boundary)::json As geometry\n" +
                    "    , row_to_json((SELECT l FROM (SELECT id, name) As l\n" +
                    "      )) As properties\n" +
                    "   FROM country As lg WHERE id=1 ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

}
