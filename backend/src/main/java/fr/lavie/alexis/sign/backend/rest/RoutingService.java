package fr.lavie.alexis.sign.backend.rest;

import fr.lavie.alexis.sign.backend.config.Database;
import fr.lavie.alexis.sign.backend.json.JsonHelper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static fr.lavie.alexis.sign.backend.tools.FileHelper.readFile;

@Path("/map/routing")
public class RoutingService {

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response routing(@DefaultValue("0") @QueryParam("x1") BigDecimal x1,
                            @DefaultValue("0") @QueryParam("y1") BigDecimal y1,
                            @DefaultValue("0") @QueryParam("x2") BigDecimal x2,
                            @DefaultValue("0") @QueryParam("y2") BigDecimal y2) {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String function = null;
        String json = null;
        String table = null;
        String query = null;

        try {
            ClassLoader classLoader = getClass().getClassLoader();
            function = readFile(getClass().getResource("/fromXtoY.sql"), StandardCharsets.UTF_8);
//            System.out.println(function);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Routing | x1: " + x1 + ", y1: " + y1 + "; x2: " + x2 + ", y2: " + y2 + ";");

        table = "ways";

        try {
            connection = Database.getRoutingConnection();
            connection.createStatement().execute(function);
            sql = connection.createStatement();
            query =
                    "SELECT row_to_json(fc)\n" +
                    " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                    "     FROM (SELECT 'Feature' As type,\n" +
                    "         ST_AsGeoJSON(ST_AddPoint(ST_AddPoint(ST_MakeLine(route.geom), ST_MakePoint(" + x1 + ", " + y1 + ")::geometry, 0), ST_MakePoint(" + x2 + ", " + y2 + ")::geometry))::json As geometry\n" +
                    "             FROM (SELECT geom FROM pgr_fromXtoY('" + table + "', " + x1 + ", " + y1 + ", " + x2 + ", " + y2 + ") ORDER BY seq ) As route ) AS f ) As fc;";
//            System.out.println(query);
            result = sql.executeQuery(query);
            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

}
