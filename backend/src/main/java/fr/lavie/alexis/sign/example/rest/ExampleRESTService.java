package fr.lavie.alexis.sign.example.rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

@Path("/example")
public class ExampleRESTService {

    @POST
    @Path("/exampleRESTService")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response exampleRESTService(InputStream incomingData) {
        StringBuilder crunchifyBuilder = new StringBuilder();

        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(incomingData));
            String line = null;

            while ((line = in.readLine()) != null) {
                crunchifyBuilder.append(line);
            }
        } catch (Exception e) {
            System.out.println("Error Parsing: - ");
        }
        System.out.println("Data Received: " + crunchifyBuilder.toString());

        // Return HTTP response 200 in case of success.
        return Response.status(200).entity(crunchifyBuilder.toString()).build();
    }

    /*
        Example : http://localhost:8080/rest/verifyExampleRESTService
     */
    @GET
    @Path("/verifyExampleRESTService")
    @Produces(MediaType.TEXT_PLAIN)
    public Response verifyExampleRESTService() {
        String result = "ExampleRESTService Successfully started.";

        // Return HTTP response 200 in case of success.
        return Response.status(200).entity(result).build();
    }

}
