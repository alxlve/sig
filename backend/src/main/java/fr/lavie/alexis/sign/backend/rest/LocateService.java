package fr.lavie.alexis.sign.backend.rest;

import fr.lavie.alexis.sign.backend.config.Database;
import fr.lavie.alexis.sign.backend.json.JsonHelper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigInteger;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Path("/map/locate")
public class LocateService {

    @GET
    @Path("/point")
    @Produces(MediaType.APPLICATION_JSON)
    public Response point(@QueryParam("id") BigInteger id) {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        geometry = "way";
        attributes = "osm_id, name";
        table = "planet_osm_point";
        where = "osm_id='" + id + "'";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , ST_AsGeoJSON(" + geometry + ")::json As geometry\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/points")
    @Produces(MediaType.APPLICATION_JSON)
    public Response points() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        geometry = "way";
        attributes = "osm_id, name, about, access";
        table = "planet_osm_point";
        where = "name <> '' ";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , ST_AsGeoJSON(" + geometry + ")::json As geometry\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygon")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygon(@QueryParam("id") BigInteger id) {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        geometry = "way";
        attributes = "osm_id";
        table = "planet_osm_polygon";
        where = "osm_id='" + id + "'";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , ST_AsGeoJSON(ST_Centroid(" + geometry + "))::json As geometry\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/autres")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsAutres() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "osm_id IN (39569054, 376398461, 376398527, 376398495," +
                "199105213, 199105211, 41688103, 200711285, 376398520," +
                "376398540, 181303196, 41688110, 40831060, -4120647," +
                "133876383, 133876384, 376398475, 376398502, 133876386," +
                "133876385, 361415459, 133876382, 361415460, 376398463," +
                "39837546)";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/bibliotheque")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsBibliotheque() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "amenity='library' OR osm_id=-275484";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/collegium-deg")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsCollegiumDEG() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "osm_id IN (-275484, -275485, 95541108)";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/collegium-llsh")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsCollegiumLLSH() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "osm_id IN (95533437, 31139277)";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/collegium-st")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsCollegiumST() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "osm_id IN (41688109, 41265558, 40831055, 41688122," +
                "41688123, 41688127, -275483, 31139267, 376398469," +
                "-275482, 41688114, 31139273, 41688121, -1717203)";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/iut")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsIUT() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "osm_id IN (40794193, 40794231, 39569041, 39569040," +
                "39569039, 41688134, 39569037, 376398478, 39569043," +
                "39569044, 39569045)";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/polytech")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsPolytech() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "osm_id IN (41688115, 41688116, 41688117, 41688118," +
                "41688119, 40876105)";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/residences")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsResidences() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;

        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "osm_id IN (40831056, 41265559, 39838231, 39838230," +
                "39837553, 39837550)";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

    @GET
    @Path("/polygons/restaurant")
    @Produces(MediaType.APPLICATION_JSON)
    public Response polygonsRestaurant() {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String table = null;
        String where = null;
//
        attributes = "osm_id, name";
        table = "planet_osm_polygon";
        where = "osm_id IN (39569056, 40831059, 95539405)";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
//                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return Response.status(200).entity(json).build();
    }

}
