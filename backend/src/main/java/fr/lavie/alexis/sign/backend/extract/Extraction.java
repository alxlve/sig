package fr.lavie.alexis.sign.backend.extract;

import fr.lavie.alexis.sign.backend.config.Database;
import fr.lavie.alexis.sign.backend.json.JsonHelper;

import java.sql.*;

public class Extraction {

    public static void main(String[] args) {
        extract("planet_osm_line");
        extract("planet_osm_point");
        extract("planet_osm_polygon");
        extract("planet_osm_roads");
    }

    public static void extract(String table) {
        Connection connection = null;
        Statement sql = null;
        ResultSet result = null;
        String json = null;
        String geometry = null;
        String attributes = null;
        String where = null;

        json = new String();

        geometry = "way";
        attributes = "osm_id, name";
        where = "1 = 1";

        try {
            connection = Database.getConnection();
            sql = connection.createStatement();
            result = sql.executeQuery(
                    "SELECT row_to_json(fc)\n" +
                            " FROM (SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
                            " FROM (SELECT 'Feature' As type\n" +
                            "    , ST_AsGeoJSON(" + geometry + ")::json As geometry\n" +
                            "    , row_to_json((SELECT l FROM (SELECT " + attributes + ") As l\n" +
                            "      )) As properties\n" +
                            "   FROM " + table + " As lg WHERE " + where + " ) As f ) As fc;");

            if (result.next()) {
                json = JsonHelper.toPrettyFormat(result.getString(1));
                System.out.println(json);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
